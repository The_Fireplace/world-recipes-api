package the_fireplace.worldrecipes;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Instance;

@Mod(modid="worldrecipesapi", name="World Recipes API", version="0.1.0", dependencies="required-after:NotEnoughItems")
public class WorldRecipes {
	@Instance("worldrecipesapi")
	public static WorldRecipes instance;

}
